from django.urls import path
from .views import index, data

app_name = 'story8'

urlpatterns = [
    path('', index, name='index'),
    path('data/', data, name='data'),
]
