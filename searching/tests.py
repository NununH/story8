from django.http import request
from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import reverse, resolve
from .views import *

# Create your tests here.

class UnitTest(TestCase):

    def test_library_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_data_url_is_exist(self):
        response = Client().get('/data/')
        self.assertEqual(response.status_code, 200)

    def test_library_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_index_contains_greeting(self):
        response = self.client.get('/')
        response_content = response.content.decode('utf-8')
        self.assertIn("What are You Looking For?", response_content)
