from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout

def index(request):
	context - {
	'page_title':'home'
	}
	template_name = None
	if request.user.is_authenticated:
		template_name = 'index.html'
	else:
		template_name = 'index_anonym.html'
	return render(request, template_name, context)

def loginView(request):
	if request.method == "POST":
		username_login = request.POST['username']
		password_login = request.POST['password']

		user = authenticate(request, username=username_login, password=password_login)

		if user is not None:
			login(request, user)
			return redirect('/')
		else:
			return render(request, 'login_error.html')
	if request.method == "GET":
		if request.user.is_authenticated:
			return redirect('/')
		else:
			return render(request, 'login.html')

def logoutView(request):
	if request.method == "POST":
		if request.POST["logout"] == "Submit":
			logout(request)
			return redirect('/')
	if request.method == "GET":
		if request.user.is_authenticated:
			return render(request, 'logout.html')
		else:
			return redirect('/')